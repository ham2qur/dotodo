$(document).ready(function() {
	// Wrap content
	WrapContent();

});

// This code will be used to animate a title bg HammadHQ Log
// function animateCloud() {
	// $("#cloud").animate({
		// 'left' : '900px'
	// }, 180000, 'linear', function() {
		// $(this).css('left', '-66px');
		// animateCloud();
	// });
// }

function WrapContent() {
	// Capture the current content
	var content = $("body").html();

	// Create the outer most wrapper
	var wrap = $("<div id='wrapper'></div>");

	// Create a header and add it to the outer wrapper
	var header = $("<div id='wrapper'><div id='header'>" +
		'<img id="topImg_dash" src="img/dotodo.png"/>'+
		"<div id='cloud' style='left: -64.1914px;'></div><span id='title'>DO" +
		"<span class='odd-word'>todo</span></span><span id='subtitle'>"+ 
		"Simply Kanbanize</span></div>");
	wrap.append(header);

	// Create the content wrap, add the content, and then add the whole thing to the outer wrapper
	var contentWrap = $("<div id='content-wrap'></div>");
	contentWrap.append(content);
	wrap.append(contentWrap);

	// Replace the current content with the wrapped content
	$("body").html(wrap);
}


